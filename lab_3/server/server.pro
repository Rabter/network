TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ThreadPool.cpp \
    server.cpp \

HEADERS += \
    ../server.h \
    ThreadPool.hpp \
    not_found.h \

LIBS += -lpthread
