#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>

#include "info.h"

long long change_base(int dec, int new_base)
{
    long long res;
    char sign;

    if (dec > 0)
        sign = 1;
    else
    {
        dec *= -1;
        sign = -1;
    }

    res = dec % new_base;
    int mul = 10;
    while (dec /= new_base)
    {
        res = res + dec % new_base * mul;
        mul *= 10;
    }
    res *= sign;

    return res;
}

void to_hex(char *res, int dec)
{
    char sign;

    if (dec > 0)
        sign = 1;
    else
    {
        dec *= -1;
        sign = -1;
    }

    int num = dec % 16;
    res[0] = (num > 9? 'A' + num - 10 : '0' + num);

    int i = 1;
    while (dec /= 16)
    {
        num = dec % 16;
        res[i++] = (num > 9? 'A' + num - 10 : '0' + num);
    }

    if (sign < 0)
        res[i++] = '-';

    int half = i / 2;
    --i;
    for (int j = 0; j < half; ++j)
    {
        char tmp = res[j];
        res[j] = res[i - j];
        res[i - j] = tmp;
    }
    res[i + 1] = 0;
}

int my_stoi(char *str, int *res)
{
    char sign = (str[0] == '-'? -1 : 1);
    int i = 0;
    if (str[0] == '-' || str[0] == '+')
        i = 1;
    *res = 0;

    while (str[i])
    {
        if (str[i] < '0' || str[i] > '9')
            return -1;
        *res = *res * 10 + str[i] - '0';
        ++i;
    }

    *res *= sign;
    return 0;
}

int main(void)
{
    sockaddr_in server_addr, client_addr;
    int sock;
    socklen_t slen = sizeof(client_addr);
    char buf[MAX_NUMLEN], hex[MAX_NUMLEN];

    printf("Server has started\n");

    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        perror("'socket()' failed");
        exit(1);
    }

    memset((char *)&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SOCK_PORT);
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sock, (sockaddr*)&server_addr, sizeof(server_addr)) == -1)
    {
        perror("'bind()' failed");
        exit(1);
    }

    if (recvfrom(sock, buf, MAX_NUMLEN, 0, (sockaddr*)&client_addr, &slen) == -1)
    {
        perror("'recvfrom()' failed");
        exit(1);
    }
    int num;
    my_stoi(buf, &num);
    to_hex(hex, num);
    printf("2: %lld\n"
           "8: %lld\n"
           "16: %s\n",
           change_base(num, 2), change_base(num, 8), hex);

    close(sock);
    return 0;
}
