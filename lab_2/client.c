#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>

#include "info.h"

int main(void)
{
    struct sockaddr_in server_addr;
    int sock, slen = sizeof(server_addr);
    char buf[MAX_NUMLEN];

    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        perror("Failed to create a socket");
        exit(1);
    }

    memset((char *)&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(SOCK_PORT);

    if (inet_aton(SRV_IP, &server_addr.sin_addr) == 0)
    {
        perror("error in inet_aton()");
        exit(1);
    }

    printf("Input number: ");
    
    scanf_s("%s", buf, sizeof(buf));
    if (sendto(sock, buf, MAX_NUMLEN, 0, (sockaddr*)&server_addr, slen) == -1)
    {
        perror("Failed to send a message");
        exit(1);
    }

    close(sock);
    return 0;
}
