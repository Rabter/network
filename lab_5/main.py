import os
import sys
import smtplib
import mimetypes
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.audio import MIMEAudio
from email.mime.multipart import MIMEMultipart


def crash(err):
    print(err)
    sys.exit(1)


class Email:
    receiver = sender = password = None
    msg = None

    def __attach_file(self, file):
        filename = os.path.basename(file)
        ctype, encoding = mimetypes.guess_type(file)
        if ctype is None or encoding is not None:
            ctype = 'application/octet-stream'
        maintype, subtype = ctype.split('/', 1)
        if maintype == 'text':
            with open(file) as fp:
                attachment = MIMEText(fp.read(), _subtype=subtype)
        elif maintype == 'image':
            with open(file, 'rb') as fp:
                attachment = MIMEImage(fp.read(), _subtype=subtype)
        elif maintype == 'audio':
            with open(file, 'rb') as fp:
                attachment = MIMEAudio(fp.read(), _subtype=subtype)
        else:  # Неизвестный тип файла
            with open(file, 'rb') as fp:
                attachment = MIMEBase(maintype, subtype)
                attachment.set_payload(fp.read())
                encoders.encode_base64(attachment)  # Содержимое должно кодироваться как Base64
        attachment.add_header('Content-Disposition', 'attachment', filename=filename)
        self.msg.attach(attachment)

    def send(self, receiver, sender, password, subject="", text="", files=None):
        if files is None:
            files = list()
        self.msg = MIMEMultipart()
        self.msg['Subject'] = subject
        self.msg.attach(MIMEText(text, 'plain'))

        self.receiver = self.msg["To"] = receiver
        self.sender = self.msg["From"] = sender
        self.password = password

        for f in files:
            if os.path.isfile(f):
                self.__attach_file(f)

        pos = sender.find('@')
        if pos == -1:
            crash("Wrong sender address")

        provider = sender[pos + 1:]
        if provider == "mail.ru":
            self.__send_mail()
        elif provider == "yandex.ru":
            self.__send_yandex()
        elif provider == "gmail.com":
            self.__send_gmail()
        else:
            print("Unknown email provider, trying default settings...")
            self.__send_default(provider)

    def __send_mail(self):
        server = smtplib.SMTP("smtp.mail.ru", 2525)
        server.starttls()
        server.login(self.sender, self.password)
        server.send_message(self.msg)
        server.quit()

    def __send_gmail(self):
        server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server.login(self.sender, self.password)
        server.send_message(self.msg)
        server.quit()

    def __send_yandex(self):
        server = smtplib.SMTP_SSL("smtp.yandex.ru", 465)
        server.login(self.sender, self.password)
        server.send_message(self.msg)
        server.quit()

    def __send_default(self, provider):
        server = smtplib.SMTP("smtp." + provider, 25)
        server.login(self.sender, self.password)
        server.send_message(self.msg)
        server.quit()


if __name__ == '__main__':

    if len(sys.argv) != 5:
        if len(sys.argv) != 1:
            crash("Wrong arguments amount")
        receiver = input("Enter receiver email address: ")
        sender = input("Enter sender email address: ")
        password = input("Enter sender password: ")
        keyword = input("Enter keyword: ")
    else:
        [receiver, sender, password, keyword] = sys.argv[1:]

    files = []
    keyword = keyword.upper()
    for fname in os.listdir():
        file_extension = os.path.splitext(fname)[1]
        if file_extension == '.txt':
            with open(fname, "r") as file:
                not_found = True
                line = file.readline()
                while line != "" and not_found:
                    if line.upper().find(keyword) != -1:
                        files.append(fname)
                        not_found = False
                    else:
                        line = file.readline()

    mail = Email()
    mail.send(receiver, sender, password, subject="SMTP message", text="Message text", files=files)
